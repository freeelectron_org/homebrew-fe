# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://rubydoc.brew.sh/Formula

# to drop local edits: brew update-reset `brew --prefix`/Library/Taps/freeelectron_org/homebrew-fe

class Freeelectron < Formula
  desc "Open Source Extensible Architecture"
  homepage "https://gitlab.com/freeelectron_org/fe"
  url "https://gitlab.com/freeelectron_org/fe.git", branch: "dev"
  version "0.0"
  # sha256 ""
  license "BSD-2-Clause"
  revision 9

  depends_on "python@3.9" => [:build, :test]
  depends_on "yaml-cpp" => :recommended

  def install
    system "python3", "build.py", "terminal", "info", "tolerant"
    rm_rf "lib/ccache"
    #cp_r "/home/symhost/sym/proj/gitlab/fe/lib", "lib"

    prefix_lib = Pathname(prefix/"lib")
    fe_lib_path = Pathname(Dir["lib/*_optimize"][0])

    mkdir_p prefix_lib
    prefix_lib.install fe_lib_path

    fe_binaries = Pathname(Dir[prefix_lib/"*_optimize"][0])
    #fe_binaries_pyfe = fe_binaries/"pyfe"
    fe_install_pyfe = Pathname(prefix/"lib/python3.9/site-packages/pyfe")

    mkdir_p fe_install_pyfe
    #ln_s Dir[fe_binaries/"*.dylib", fe_binaries/"*.so", fe_binaries/"*.yaml"], fe_binaries_pyfe, force: true
    #ln_s Dir[fe_binaries/"pyfe/*.py"], fe_install_pyfe, force: true
    ln_s Dir[fe_binaries/"*.py"], fe_install_pyfe, force: true
  end

  test do
    print "#{bin}\n"

    prefix_lib = Pathname(prefix/"lib")
    fe_lib_path = Pathname(Dir[prefix_lib/"*_optimize"][0])

    ENV.append "LD_LIBRARY_PATH", fe_lib_path
    system fe_lib_path/"inspect.exe", "feDataDL"
  end
end
