# homebrew-fe

brew formula for FE

## Install

To download and build,

```bash
brew tap freeelectron_org/fe https://gitlab.com/freeelectron_org/homebrew-fe
brew install freeelectron
```

You should only need to tap once and
thereafter just upgrade (or reinstall) as needed.

```bash
brew update
brew upgrade [freeelectron]
```

## Usage

If you don't use a homebrew build of python,
you may have to set up PYTHONPATH manually.

```bash
python3.9

import pyfe.context
fe = pyfe.context.Context()

help(pyfe.context)
```

## Disable

To hide, while keeping the build,

```bash
brew unlink freeelectron
```

To reenable,

```bash
brew link freeelectron
```

## Uninstall

To discard the build,

```bash
brew uninstall freeelectron
```
